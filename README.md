<a href="https://k-tec.uk/" alt="K-tec UK web site"><img src="https://k-tec.uk/wp-content/uploads/2022/04/logo-colour-cropped-max.svg" alt="K-tec UK logo" width="50%" /></a>

# PSP game settings for RK3566 devices (Anbernic RG503/RG353...) #

A project by [K-tec UK](https://k-tec.uk/).

Inspired by https://github.com/jserodio/rg351p-ppsspp-settings

This repository contains game-specific configurations which aim to provide the best available performance on RK3566-based devices, such as the [Anbernic RG503](https://k-tec.uk/product/anbernic-rg503-retro-handheld/) and RG353P/V/VS. So far, these have been tested on an [Anbernic RG503](https://k-tec.uk/product/anbernic-rg503-retro-handheld/) running [ArkOS](https://github.com/christianhaitian/arkos/wiki).

Please contribute your own settings. We want this repository to cover a wide range of games, and to be useful to as many people as possible. See [Contributing](#Contributing) below.

## Installation ##

Copy the `*.ini` files from this repository's `SYSTEM` folder into the `PSP/SYSTEM/` folder of your ppsspp configuration. If you're running ArkOS, this is `/psp/ppsspp/PSP/SYSTEM/` on the card/partition containing your ROMs.

We also suggest that you configure PPSSPP with the system-wide settings we describe in [summaries/Default.md](summaries/Default.md).

## Games that work well with our default settings ##

We have some PPSSPP settings we suggest you configure as system-wide defaults. See  [summaries/Default.md](summaries/Default.md).

The following games have tested well with these default settings so they don't have per-game congifurations in this repository.

- Disgaea - Afternoon of Darkness
- LittleBigPlanet
- LocoRoco
- Lumines
- Pilot Academy
- PixelJunk Monsters - Deluxe

## Unplayable games ##

These are games that we found to be unplayable no matter which settings we configured. If you find a configuration that works, please contribute it to this project.

- Tactics Ogre - Let Us Cling Together

## Contributing ##

Please improve this repository by adding settings for new games, or further optimizing an existing configuration. Submit a Merge Request with the game's `.ini` file in the `SYSTEM` folder, and a summary of those settings in the `summaries` folder.

You can also create an issue if you want to discuss the settings, but you're not yet ready to create a Merge Request.

## Acknowledgements ##

[Jose Serodio](https://github.com/jserodio) for inspiring this project with his [rg351p-ppsspp-settings repository](https://github.com/jserodio/rg351p-ppsspp-settings).

[The Gaming Geek](https://www.thegaminggeek.net/anbernic-rg503-psp-compatibility-guide/) for the setting hints that provided the starting point for our testing.

## Licence ##

This project uses the MIT Licence.
