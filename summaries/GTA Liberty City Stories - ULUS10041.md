# GTA: Liberty City Stories #

Game code: __ULUS10041__

## Graphics ##

### Rendering mode ###

| Setting | Value              |
|---------|--------------------|
| Mode    | Buffered rendering |

### Framerate control ###

| Setting             | Value            |
|---------------------|------------------|
| Frame skipping      | 1                |
| Frame skipping type | Number of frames |
| Auto frameskip      | Enabled          |
| Force Max FPS       | 30               |

### Performance ###

| Setting              | Value  |
|----------------------|--------|
| Rendering resolution | 2x PSP |

## System ##

### Emulation ###

| Setting                         | Value |
|---------------------------------|-------|
| Change emulated PSP's CPU clock | 1000  |
