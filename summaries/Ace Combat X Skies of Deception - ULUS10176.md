# Ace Combat X - Skies of Deception #

Game code: __ULUS10176__

## Graphics ##

### Rendering mode ###

| Setting | Value              |
|---------|--------------------|
| Mode    | Buffered rendering |

### Framerate control ###

| Setting             | Value            |
|---------------------|------------------|
| Frame skipping      | 1                |
| Frame skipping type | Number of frames |
| Auto frameskip      | Enabled          |
| Force Max FPS       | Auto             |

### Performance ###

| Setting              | Value  |
|----------------------|--------|
| Rendering resolution | 2x PSP |

## System ##

### Emulation ###

| Setting                         | Value |
|---------------------------------|-------|
| Change emulated PSP's CPU clock | 666   |
