# Metal Gear Solid Portable Ops #

Game code: __ULUS10202__

## Graphics ##

### Rendering mode ###

| Setting | Value              |
|---------|--------------------|
| Mode    | Buffered rendering |

### Framerate control ###

| Setting             | Value            |
|---------------------|------------------|
| Frame skipping      | Off              |
| Frame skipping type | Number of frames |
| Auto frameskip      | Disabled         |
| Force Max FPS       | Auto             |

### Performance ###

| Setting              | Value  |
|----------------------|--------|
| Rendering resolution | 2x PSP |

## System ##

### Emulation ###

| Setting                         | Value |
|---------------------------------|-------|
| Change emulated PSP's CPU clock | 666   |
